
public class GameBuilder {
    public init() {
        
    }
    
    public func build(rows: Int, cols: Int) -> Game {
        let grid = Grid(rows: rows, cols: cols)
        let squares = buildSquares(grid: grid)
        let world = World(squares: squares, grid: grid)
        return Game(world: world)
    }
    
    func buildSquares(grid: Grid) -> [Square] {
        var squares = [Square]()
        for row in 0 ..< grid.rows {
            for col in 0 ..< grid.cols {
                let square = Square(row: row, col: col)
                squares.append(square)
            }
        }
        return squares
    }
}
