
public protocol GridLocatable: GridIndexable {
    var xOffset: Float { get }
    var yOffset: Float { get }
    func offsettingBy(dx: Float, dy: Float) -> GridLocatable
}

public extension GridLocatable {
    func offsettingBy(dx: Float, dy: Float) -> GridLocatable {
        var newX = xOffset + dx
        var newY = yOffset + dy
        var newRow = row
        var newCol = col
        if newX > 0.5 {
            newX -= 1
            newCol += 1
        }
        if newY > 0.5 {
            newY -= 1
            newRow += 1
        }
        if newX < -0.5 {
            newX += 1
            newCol -= 1
        }
        if newY < -0.5 {
            newY += 1
            newRow -= 1
        }
        return GridLocation(row: newRow, col: newCol, xOffset: newX, yOffset: newY)
    }
}
