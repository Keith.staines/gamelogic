

public protocol GridIndexable {
    var row: Int { get }
    var col: Int { get }
}

public struct GridIndex: GridIndexable, Codable, Equatable {
    public let row: Int
    public let col: Int
    public init(row: Int, col: Int) {
        self.row = row
        self.col = col
    }
}
