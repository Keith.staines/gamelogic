
public struct GridLocation: GridLocatable {
    public var xOffset: Float
    public var yOffset: Float
    public var row: Int
    public var col: Int
    
    public init(row: Int, col: Int, xOffset: Float = 0, yOffset: Float = 0) {
        self.row = row
        self.col = col
        self.xOffset = xOffset
        self.yOffset = yOffset
    }
}
