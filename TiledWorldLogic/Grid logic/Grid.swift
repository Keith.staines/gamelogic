
public struct Grid: Codable {
    public let rows: Int
    public let cols: Int
    
    public func linearIndex(gridIndex: GridIndexable) -> Int {
        gridIndex.row * cols + gridIndex.col
    }
    
    public func gridIndexIsInGrid(_ gridIndex: GridIndex) -> Bool {
        if gridIndex.col < 0 { return false }
        if gridIndex.col >= cols { return false }
        if gridIndex.row < 0 { return false }
        if gridIndex.row >= rows { return false }
        return true
    }
    
    public func indexAtAdjacency(_ adjacency: GridAdjacency, relativeTo index: GridIndexable?) -> GridIndex? {
        switch adjacency {
        case .northWest: return indexNorthWest(index)
        case .north: return indexNorth(index)
        case .northEast: return indexNorthEast(index)
        case .east: return indexEast(index)
        case .southEast: return indexSouthEast(index)
        case .south: return indexSouth(index)
        case .southWest: return indexSouthWest(index)
        case .west: return indexWest(index)
        }
    }
    
    public init(rows: Int, cols: Int) {
        self.rows = rows
        self.cols = cols
    }
}

extension Grid {
    private func indexWest(_ gridIndex: GridIndexable?) -> GridIndex? {
        guard
            let row = gridIndex?.row,
            let col = gridIndex?.col,
            col > 0
        else { return nil }
        return GridIndex(row: row, col: col - 1)
    }
    
    private func indexEast(_ gridIndex: GridIndexable?) -> GridIndex? {
        guard
            let row = gridIndex?.row,
            let col = gridIndex?.col,
            col < cols - 1
        else { return nil }
        return GridIndex(row: row, col: col + 1)
    }
    
    private func indexNorth(_ gridIndex: GridIndexable?) -> GridIndex? {
        guard
            let row = gridIndex?.row,
            let col = gridIndex?.col,
            row > 0
        else { return nil }
        return GridIndex(row: row - 1, col: col)
    }
    
    private func indexSouth(_ gridIndex: GridIndexable?) -> GridIndex? {
        guard
            let row = gridIndex?.row,
            let col = gridIndex?.col,
            row < rows - 1
        else { return nil }
        return GridIndex(row: row + 1, col: col)
    }
    
    private func indexNorthWest(_ gridIndex:  GridIndexable?) -> GridIndex? {
        let north = indexNorth(gridIndex)
        return indexWest(north)
    }
    
    private func indexNorthEast(_ gridIndex:  GridIndexable?) -> GridIndex? {
        let north = indexNorth(gridIndex)
        return indexEast(north)
    }
    
    private func indexSouthWest(_ gridIndex:  GridIndexable?) -> GridIndex? {
        let south = indexSouth(gridIndex)
        return indexWest(south)
    }
    
    private func indexSouthEast(_ gridIndex:  GridIndexable?) -> GridIndex? {
        let south = indexSouth(gridIndex)
        return indexEast(south)
    }
}

