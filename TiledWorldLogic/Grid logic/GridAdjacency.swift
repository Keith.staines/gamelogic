

public enum GridAdjacency {
    case northWest
    case north
    case northEast
    case east
    case southEast
    case south
    case southWest
    case west
}
