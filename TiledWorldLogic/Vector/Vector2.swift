import Foundation

public struct Vector2: Codable, Equatable {
    public static let zero = Vector2(x: 0, y: 0)
    public var x: Float
    public var y: Float
    
    public init(x: Float, y: Float) {
        self.x = x
        self.y = y
    }
    
    public var length: Float { sqrt(x*x + y*y) }
    public var lengthSquared: Float { x*x + y*y }
    
    static public func +(lhs: Vector2, rhs: Vector2) -> Vector2 {
        Vector2(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
    
    static public func -(lhs: Vector2, rhs: Vector2) -> Vector2 {
        Vector2(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
    }
    
    static public func *(lhs: Float, rhs: Vector2) -> Vector2 {
        Vector2(x: lhs*rhs.x, y: lhs*rhs.y)
    }
    
    static public func *(lhs: Vector2, rhs: Vector2) -> Float {
        lhs.x*rhs.x + lhs.y*rhs.y
    }
    
    static public func /(lhs: Vector2, rhs: Float) -> Vector2 {
        Vector2(x: lhs.x/rhs, y: lhs.y/rhs)
    }
}
