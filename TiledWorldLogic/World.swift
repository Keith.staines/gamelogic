
public class World: Codable {
    public let grid: Grid
    let squares : [Square]
    
    init(squares: [Square], grid: Grid) {
        self.grid = grid
        self.squares = squares
    }
    
    func squareAtGridIndex(_ gridIndex: GridIndexable?) -> Square? {
        guard let gridIndex = gridIndex else { return nil }
        let linearIndex = grid.linearIndex(gridIndex: gridIndex)
        return squares[linearIndex]
    }
    
    func squareAtAdjacency(_ adjacency: GridAdjacency, relativeTo gridIndex: GridIndexable) -> Square? {
        guard let gridIndex = grid.indexAtAdjacency(adjacency, relativeTo: gridIndex)
        else { return nil }
        return squareAtGridIndex(gridIndex)
    }
    
}
