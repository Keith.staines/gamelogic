//
//  WorldLocationTests.swift
//  TiledWorldLogicTests
//
//  Created by Keith Staines on 07/11/2020.
//

import XCTest
import TiledWorldLogic

class GridLocationTests: XCTestCase {

    func test_initialise_no_offset() {
        let location = GridLocation(row: 1, col: 2)
        XCTAssertEqual(location.row, 1)
        XCTAssertEqual(location.col, 2)
        XCTAssertEqual(location.xOffset, 0)
        XCTAssertEqual(location.yOffset, 0)
    }
    
    func test_initialise_with_offset() {
        let location = GridLocation(row: 1, col: 2, xOffset: 0.3, yOffset: 0.4)
        XCTAssertEqual(location.row, 1)
        XCTAssertEqual(location.col, 2)
        XCTAssertEqual(location.xOffset, 0.3)
        XCTAssertEqual(location.yOffset, 0.4)
    }
    
    func test_moveBy_zero() {
        let sut = GridLocation(row: 0, col: 0)
        let newLocation = sut.offsettingBy(dx: 0.0, dy: 0.0)
        XCTAssertEqual(newLocation.xOffset, 0.0)
        XCTAssertEqual(newLocation.yOffset, 0.0)
    }
    
    func test_moveBy_positive_1() {
        let sut = GridLocation(row: 0, col: 0)
        let newLocation = sut.offsettingBy(dx: 1.0, dy: 1.0)
        XCTAssertEqual(newLocation.row, 1)
        XCTAssertEqual(newLocation.col, 1)
        XCTAssertEqual(newLocation.xOffset, 0.0)
        XCTAssertEqual(newLocation.yOffset, 0.0)
    }
    
    func test_moveBy_negative_1() {
        let sut = GridLocation(row: 0, col: 0)
        let newLocation = sut.offsettingBy(dx: -1.0, dy: -1.0)
        XCTAssertEqual(newLocation.row, -1)
        XCTAssertEqual(newLocation.col, -1)
        XCTAssertEqual(newLocation.xOffset, 0.0)
        XCTAssertEqual(newLocation.yOffset, 0.0)
    }

}
