//
//  GridTests.swift
//  TiledWorldLogicTests
//
//  Created by Keith Staines on 07/11/2020.
//

import XCTest
import TiledWorldLogic

class GridTests: XCTestCase {
    
    func makeSUT(rowCount: Int = 3, colCount: Int = 3) -> Grid {
        Grid(rows: rowCount, cols: colCount)
    }
    
    func test_initialise() {
        let sut = makeSUT(rowCount: 4, colCount: 5)
        XCTAssertEqual(sut.rows,4)
        XCTAssertEqual(sut.cols,5)
    }

    func test_indexAtAdjacency_north() {
        let sut = makeSUT()
        let north = sut.indexAtAdjacency(.north, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(north?.row, 0)
        XCTAssertEqual(north?.col, 1)
    }
    
    func test_indexAtAdjacency_northEast() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.northEast, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 0)
        XCTAssertEqual(adjacent?.col, 2)
    }
    
    func test_indexAtAdjacency_northWest() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.northWest, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 0)
        XCTAssertEqual(adjacent?.col, 0)
    }
    
    func test_indexAtAdjacency_south() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.south, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 2)
        XCTAssertEqual(adjacent?.col, 1)
    }
    
    func test_indexAtAdjacency_southEast() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.southEast, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 2)
        XCTAssertEqual(adjacent?.col, 2)
    }
    
    func test_indexAtAdjacency_southWest() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.southWest, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 2)
        XCTAssertEqual(adjacent?.col, 0)
    }
    
    func test_indexAtAdjacency_east() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.east, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 1)
        XCTAssertEqual(adjacent?.col, 2)
    }
    
    func test_indexAtAdjacency_west() {
        let sut = makeSUT()
        let adjacent = sut.indexAtAdjacency(.west, relativeTo: GridIndex(row: 1, col: 1))
        XCTAssertEqual(adjacent?.row, 1)
        XCTAssertEqual(adjacent?.col, 0)
    }
    
    func test_off_grid_indexes() {
        let sut = makeSUT(rowCount: 1, colCount: 1)
        let index = GridIndex(row: 0, col: 0)
        XCTAssertEqual(sut.rows, 1)
        XCTAssertEqual(sut.cols, 1)
        XCTAssertNil(sut.indexAtAdjacency(.north, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.east, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.south, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.west, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.northEast, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.northWest, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.southWest, relativeTo: index))
        XCTAssertNil(sut.indexAtAdjacency(.southEast, relativeTo: index))
    }

    func test_linearIndex() {
        let sut = makeSUT(rowCount: 3, colCount: 3)
        XCTAssertEqual(sut.linearIndex(gridIndex: GridIndex(row: 0, col: 0)), 0)
        XCTAssertEqual(sut.linearIndex(gridIndex: GridIndex(row: 0, col: 2)), 2)
        XCTAssertEqual(sut.linearIndex(gridIndex: GridIndex(row: 1, col: 0)), 3)
        XCTAssertEqual(sut.linearIndex(gridIndex: GridIndex(row: 2, col: 2)), 8)
    }
    
    func test_gridIndexIsInGrid() {
        let sut = makeSUT(rowCount: 3, colCount: 3)
        XCTAssertTrue(sut.gridIndexIsInGrid(GridIndex(row: 1, col: 1)))
        XCTAssertFalse(sut.gridIndexIsInGrid(GridIndex(row: -1, col: 1)))
        XCTAssertFalse(sut.gridIndexIsInGrid(GridIndex(row: 3, col: 1)))
        XCTAssertFalse(sut.gridIndexIsInGrid(GridIndex(row: 1, col: -1)))
        XCTAssertFalse(sut.gridIndexIsInGrid(GridIndex(row: 1, col: 3)))
    }
    
}
