//
//  GridTests.swift
//  TiledWorldLogicTests
//
//  Created by Keith Staines on 07/11/2020.
//

import XCTest
import TiledWorldLogic

class Vector2Tests: XCTestCase {
    
    func test_zero() {
        XCTAssertEqual(Vector2.zero.x, 0)
        XCTAssertEqual(Vector2.zero.y, 0)
    }

    func test_initialize() {
        let sut = Vector2(x: 1, y: 2)
        XCTAssertEqual(sut.x, 1)
        XCTAssertEqual(sut.y, 2)
    }
    
    func test_length() {
        let sut = Vector2(x: 3, y: 4)
        XCTAssertEqual(sut.length, 5)
    }
    
    func test_length2() {
        let sut = Vector2(x: 3, y: 4)
        XCTAssertEqual(sut.lengthSquared, 25)
    }
    
    func test_equal() {
        let v1 = Vector2(x: 1, y: 2)
        let v2 = v1
        XCTAssertEqual(v1,v2)
    }
    
    func test_add() {
        let v1 = Vector2(x: 1, y: 2)
        let v2 = Vector2(x:4, y:5)
        XCTAssertEqual(v1+v2, Vector2(x:5,y:7))
    }
    
    func test_subtract() {
        let v1 = Vector2(x: 1, y: 2)
        let v2 = Vector2(x:4, y:5)
        XCTAssertEqual(v1-v2, Vector2(x:-3,y:-3))
    }
    
    func test_scalerMultiply() {
        let v1 = Vector2(x: 1, y: 2)
        XCTAssertEqual(2*v1, Vector2(x:2,y:4))
    }
    
    func test_scalerDivide() {
        let v1 = Vector2(x: 3, y: 6)
        XCTAssertEqual(v1/3, Vector2(x:1,y:2))
    }
    
    func test_dotProduct() {
        let v1 = Vector2(x: 1, y: 2)
        let v2 = Vector2(x:4, y:5)
        XCTAssertEqual(v1*v2, 14.0)
    }
}
