
import XCTest
@testable import TiledWorldLogic

class GameBuilderTests: XCTestCase {
    
    func test_initialise() {
        let sut = GameBuilder()
        let game = sut.build(rows: 5, cols: 6)
        XCTAssertNotNil(game)
        XCTAssertEqual(game.world.grid.rows, 5)
        XCTAssertEqual(game.world.grid.cols, 6)
        XCTAssertEqual(game.world.squares.count, 5*6)
    }
}
