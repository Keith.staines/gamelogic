//
//  GridTests.swift
//  TiledWorldLogicTests
//
//  Created by Keith Staines on 07/11/2020.
//

import XCTest
@testable import TiledWorldLogic

class WorldTests: XCTestCase {
    
    let squares = [
        Square(row: 0, col: 0),Square(row: 0, col: 1),Square(row: 0, col: 2),
        Square(row: 1, col: 0),Square(row: 1, col: 1),Square(row: 1, col: 2),
        Square(row: 2, col: 0),Square(row: 2, col: 1),Square(row: 2, col: 2),
    ]
    let grid = Grid(rows: 3, cols: 3)
    
    func makeSUT() -> World {
        World(squares: squares, grid: grid)
    }
    
    func test_initialise() {
        let sut = makeSUT()
        XCTAssertEqual(sut.grid.rows,3)
        XCTAssertEqual(sut.grid.cols,3)
    }
    
    func test_squareAtGridIndex() {
        let sut = makeSUT()
        XCTAssertEqual(sut.squareAtGridIndex(GridIndex(row: 1, col: 2))?.row,1)
        XCTAssertEqual(sut.squareAtGridIndex(GridIndex(row: 1, col: 2))?.col,2)
    }

    func test_squareAtAdjacency_when_null() {
        let sut = makeSUT()
        XCTAssertNil(sut.squareAtAdjacency(.north, relativeTo: GridIndex(row: 0, col: 0)))
    }
    
    func test_squareAtAdjacency_when_not_null() {
        let sut = makeSUT()
        XCTAssertNotNil(sut.squareAtAdjacency(.south, relativeTo: GridIndex(row: 0, col: 0)))
    }
}
